<?php

use Aedart\Model\Email\Interfaces\EmailAware;
use Aedart\Model\Email\Traits\EmailTrait;
use Faker\Factory as FakerFactory;

/**
 * Class EmailTraitTest
 *
 * @coversDefaultClass Aedart\Model\Email\Traits\EmailTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class EmailTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Model\Email\Interfaces\EmailAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Model\Email\Traits\EmailTrait');
        return $m;
    }

    /**
     * Get a dummy class
     *
     * @return EmailAware
     */
    protected function getDummyClass(){
        return new DummyEmailClass();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @covers ::getDefaultEmail
     * @covers ::getEmail
     * @covers ::hasEmail
     * @covers ::hasDefaultEmail
     */
    public function getDefaultEmail(){
        $trait = $this->getTraitMock();
        $this->assertNull($trait->getEmail());
    }

    /**
     * @test
     *
     * @covers ::getDefaultEmail
     * @covers ::getEmail
     * @covers ::hasEmail
     * @covers ::hasDefaultEmail
     * @covers ::setEmail
     * @covers ::isEmailValid
     */
    public function getDefaultEmailFromCustomImpl(){
        $dummy = $this->getDummyClass();
        $this->assertNotNull($dummy->getEmail());
    }

    /**
     * @test
     *
     * @covers ::getEmail
     * @covers ::setEmail
     * @covers ::hasEmail
     * @covers ::isEmailValid
     */
    public function setAndGetEmail(){
        $trait = $this->getTraitMock();

        $email = $this->faker->email;

        $trait->setEmail($email);

        $this->assertSame($email, $trait->getEmail());
    }

    /**
     * @test
     *
     * @covers ::getEmail
     * @covers ::setEmail
     * @covers ::hasEmail
     * @covers ::isEmailValid
     *
     * @expectedException \Aedart\Model\Email\Exceptions\InvalidEmailException
     */
    public function attemptSetInvalidEmail(){
        $trait = $this->getTraitMock();

        $email = $this->faker->word;

        $trait->setEmail($email);
    }

}

class DummyEmailClass implements EmailAware {
    use EmailTrait;

    public function getDefaultEmail() {
        return 'myDefault@mail.com';
    }

}