<?php

use Aedart\Model\Email\Validators\EmailValidator;

/**
 * Class EmailValidatorTest
 *
 * @coversDefaultClass Aedart\Model\Email\Validators\EmailValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class EmailValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult, $extraDesc
             */

            /*
             * Values to be tested are inspired from http://blogs.msdn.com/b/testing123/archive/2009/02/05/email-address-test-cases.aspx
             */

            /*
             * Valid
             */
            ['email@domain.com', [], true, 'Valid email'],
            ['firstname.lastname@domain.com', [], true, 'Email contains dot in the address field'],
            ['email@subdomain.domain.com', [], true, 'Email contains dot with subdomain'],
            ['firstname+lastname@domain.com', [], true, 'Plus sign is considered valid character'],
            //['email@123.123.123.123', [], true, 'Domain is valid IP address'], // FAILS - FILTER_VALIDATE_EMAIL appears not to allow ips stated like this! But allows the next one (email@[123.123.123.123])!
            ['email@[123.123.123.123]', [], true, 'Square bracket around IP address is considered valid'],
            ['"email"@domain.com', [], true, 'Quotes around email is considered valid'],
            ['1234567890@domain.com', [], true, 'Digits in address are valid'],
            ['email@domain-one.com', [], true, 'Dash in domain name is valid'],
            ['_______@domain.com', [], true, 'Underscore in the address field is valid'],
            ['email@domain.name', [], true, '.name is valid Top Level Domain name'],
            ['email@domain.co.jp', [], true, 'Dot in Top Level Domain name also considered valid (use co.jp as example here)'],
            ['firstname-lastname@domain.com', [], true, 'Dash in address field is valid'],

            /*
             * Invalid
             */
            ['plainaddress', [], false, 'Missing @ sign and domain'],
            ['#@%^%#$@#$@#.com', [], false, 'Garbage'],
            ['@domain.com', [], false, 'Missing username'],
            ['Joe Smith <email@domain.com>', [], false, 'Encoded html within email is invalid'],
            ['email.domain.com', [], false, 'Missing @'],
            ['email@domain@domain.com', [], false, 'Two @ sign'],
            ['.email@domain.com', [], false, 'Leading dot in address is not allowed'],
            ['email.@domain.com', [], false, 'Trailing dot in address is not allowed'],
            ['email..email@domain.com', [], false, 'Multiple dots'],
            ['あいうえお@domain.com', [], false, 'Unicode char as address'],
            ['email@domain.com (Joe Smith)', [], false, 'Text followed email is not allowed'],
            ['email@domain', [], false, 'Missing top level domain (.com/.net/.org/etc)'],
            ['email@-domain.com', [], false, 'Leading dash in front of domain is invalid'],
            //['email@domain.web', [], false, '.web is not a valid top level domain'], // FAILS - FILTER_VALIDATE_EMAIL doesn't know all tdl / care about them!
            ['email@111.222.333.44444', [], false, 'Invalid IP format'],
            ['email@domain..com', [], false, 'Multiple dot in the domain portion is invalid'],

            /*
             * No options supported by this validator
             */
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     * @param string $extraDesc
     */
    public function isValid($value, $validateOptions, $expectedResult, $extraDesc){
        $result = EmailValidator::isValid($value, $validateOptions);

        $shouldBeValidMsg = 'valid';
        if($expectedResult === false){
            $shouldBeValidMsg = 'invalid';
        }

        $failMsg = sprintf(
            'Failed validating that %s is %s, with the following options %s. Additional info; %s',
            var_export($value, true),
            $shouldBeValidMsg,
            var_export($validateOptions, true),
            $extraDesc
        );

        $this->assertSame($expectedResult, $result, $failMsg);
    }

}