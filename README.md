## Model-Email ##

Getter and Setter package for a model email

This package is part of the Aedart\Model namespace; visit https://bitbucket.org/aedart/model to learn more about the project.

Official sub-package website (https://bitbucket.org/aedart/model-email)

## Contents ##

[TOC]

## When to use this ##

When your component(s) need to be aware of an email

## How to install ##

```
#!console

composer require aedart/model-email 1.*
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##

Provided that you have an interface, e.g. for a person, you can extend the email-aware interface;

```
#!php
<?php
use Aedart\Model\Email\Interfaces\EmailAware;

interface IPerson extends EmailAware {

    // ... Remaining interface implementation not shown ...
    
}

```

In your concrete implementation, you simple use the email-traits;
 
```
#!php
<?php
use Aedart\Model\Email\Traits\EmailTrait;

class MyPerson implements IPerson {
 
    use EmailTrait;

    // ... Remaining implementation not shown ... 
 
}
```

## Acknowledgement ##

[Chan Chaiyochlarb, et al., 2009](http://blogs.msdn.com/b/testing123/archive/2009/02/05/email-address-test-cases.aspx), for publishing some very nice email test cases, which have been applied in this package.

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package

