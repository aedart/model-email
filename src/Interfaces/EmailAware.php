<?php  namespace Aedart\Model\Email\Interfaces;

use Aedart\Model\Email\Exceptions\InvalidEmailException;

/**
 * Interface Email Aware
 *
 * Components, classes or objects that implements this interface, promise that a given email can be specified and retrieved.
 * Furthermore, depending upon implementation, a default email might be returned, if no email has been set prior to obtaining it.
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Email\Interfaces
 */
interface EmailAware {

    /**
     * Set the email
     *
     * @param string $email The email to be used by this component
     *
     * @return void
     *
     * @throws InvalidEmailException If given email is invalid
     */
    public function setEmail($email);

    /**
     * Get the email
     *
     * If no email has been specified, this method sets and
     * returns a default email, if one is available
     *
     * @return string|null The set email or null if none is available
     */
    public function getEmail();

    /**
     * Get a default email, to be used if no email is specified
     *
     * @return string|null A default email Or null if no default is available
     */
    public function getDefaultEmail();

    /**
     * Check if an email was set
     *
     * @return bool True if an email was set, false if not
     */
    public function hasEmail();

    /**
     * Check if a default email is available
     *
     * @return bool True if a default email is available, false if not
     */
    public function hasDefaultEmail();

    /**
     * Check if the given email is valid or not
     *
     * @param mixed $email The email to be validated
     *
     * @return bool True if given email is valid, false if not
     */
    public function isEmailValid($email);
}