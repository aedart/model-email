<?php  namespace Aedart\Model\Email\Validators;
use Aedart\Validate\BaseValidator;

/**
 * Class Email Validator
 *
 * <br />
 *
 * Validate if the given value is an email, acc. to RFC 822 standard
 *
 * @see http://www.w3.org/Protocols/rfc822/
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Email\Validators
 */
class EmailValidator extends BaseValidator{

    public function validate() {
        $value = $this->getValidateValue();
        return (bool) filter_var($value, FILTER_VALIDATE_EMAIL);
    }
}