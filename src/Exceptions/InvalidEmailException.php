<?php  namespace Aedart\Model\Email\Exceptions; 

/**
 * Class Invalid Email Exception
 *
 * Throw this exception when an invalid email has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Email\Exceptions
 */
class InvalidEmailException extends \InvalidArgumentException{

}