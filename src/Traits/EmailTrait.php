<?php  namespace Aedart\Model\Email\Traits; 

use Aedart\Model\Email\Exceptions\InvalidEmailException;
use Aedart\Model\Email\Validators\EmailValidator;

/**
 * Trait Email
 *
 * @see EmailAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Email\Traits
 */
trait EmailTrait {

    /**
     * This component's email
     *
     * @var string|null
     */
    protected $email = null;

    /**
     * Set the email
     *
     * @param string $email The email to be used by this component
     *
     * @return void
     *
     * @throws InvalidEmailException If given email is invalid
     */
    public function setEmail($email){
        if(!$this->isEmailValid($email)){
            throw new InvalidEmailException(sprintf('%s is not a valid email', var_export($email, true)));
        }
        $this->email = $email;
    }

    /**
     * Get the email
     *
     * If no email has been specified, this method sets and
     * returns a default email, if one is available
     *
     * @return string|null The set email or null if none is available
     */
    public function getEmail(){
        if(!$this->hasEmail() && $this->hasDefaultEmail()){
            $this->setEmail($this->getDefaultEmail());
        }
        return $this->email;
    }

    /**
     * Get a default email, to be used if no email is specified
     *
     * @return string|null A default email Or null if no default is available
     */
    public function getDefaultEmail(){
        return null;
    }

    /**
     * Check if an email was set
     *
     * @return bool True if an email was set, false if not
     */
    public function hasEmail(){
        if(!is_null($this->email)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default email is available
     *
     * @return bool True if a default email is available, false if not
     */
    public function hasDefaultEmail(){
        if(!is_null($this->getDefaultEmail())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given email is valid or not
     *
     * @param mixed $email The email to be validated
     *
     * @return bool True if given email is valid, false if not
     */
    public function isEmailValid($email){
        return EmailValidator::isValid($email);
    }

}